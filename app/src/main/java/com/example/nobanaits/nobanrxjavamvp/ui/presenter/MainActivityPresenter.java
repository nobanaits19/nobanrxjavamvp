package com.example.nobanaits.nobanrxjavamvp.ui.presenter;

import android.util.Log;

import com.example.nobanaits.nobanrxjavamvp.model.ClientList;
import com.example.nobanaits.nobanrxjavamvp.network.api.Reposetry;
import com.example.nobanaits.nobanrxjavamvp.ui.Contract.MainActivityContract;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class MainActivityPresenter implements MainActivityContract.presenter {
    private String TAG = "MainPresenter";
    MainActivityContract.view view;
    private Consumer<? super Throwable> onError;

    public MainActivityPresenter(MainActivityContract.view view) {
        this.view = view;
    }

    @Override
    public void getClient() {
        Reposetry.getClient()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::displayClients,  throwable -> onError(throwable.getMessage()));
    }

    private void displayClients(ClientList clientList) {
        view.displayClients(clientList);
    }

    private void onError(String message) {
        Log.e(TAG, "onError: " + message );
        view.displayError(message);
    }

    public DisposableObserver<ClientList> getObserver(){
        return new DisposableObserver<ClientList>() {
            @Override
            public void onNext(ClientList clientList) {
                Log.d(TAG,"OnNext"+clientList.getClient());
                view.displayClients(clientList);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG,"Error"+e);
                e.printStackTrace();
                view.displayError("Error fetching Data");
            }

            @Override
            public void onComplete() {
                Log.d(TAG,"Completed");
            }
        };
    }
}
