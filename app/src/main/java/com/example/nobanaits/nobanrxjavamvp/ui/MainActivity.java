package com.example.nobanaits.nobanrxjavamvp.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.nobanaits.nobanrxjavamvp.R;
import com.example.nobanaits.nobanrxjavamvp.adapter.ClientsAdapter;
import com.example.nobanaits.nobanrxjavamvp.model.ClientList;
import com.example.nobanaits.nobanrxjavamvp.network.api.RetrofitSingletone;
import com.example.nobanaits.nobanrxjavamvp.ui.Contract.MainActivityContract;
import com.example.nobanaits.nobanrxjavamvp.ui.presenter.MainActivityPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainActivityContract.view {

    @BindView(R.id.client_recykler)
    RecyclerView recyclerView;
    MainActivityPresenter presenter;
    ClientsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initPresenter();
        setupViews();
        presenter.getClient();
    }



    @Override
    public void showToast(String s) {
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayClients(ClientList clientList) {
        if (clientList != null) {

            adapter = new ClientsAdapter(clientList.getClient(), R.layout.clients_item, MainActivity.this);
            recyclerView.setAdapter(adapter);
        } else {
            Log.d("NN", "Response null");
        }
    }

    @Override
    public void displayError(String s) {
        showToast(s);
    }


    private void initPresenter() {
        presenter = new MainActivityPresenter(this);
    }

    private void setupViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
