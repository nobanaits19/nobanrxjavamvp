package com.example.nobanaits.nobanrxjavamvp.network.api;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSingletone {

    private static String HOST = "http://audacityit.com/profile/api/";

    private static Retrofit mRetrofit;

    public synchronized static Retrofit getInstance(Context context) {
        if (mRetrofit == null) {
            createRetrofit(context);
        }
        return mRetrofit;
    }

    public synchronized static Retrofit getInstance() {
        return mRetrofit;
    }

    public synchronized static void setmRetrofit() {
        mRetrofit = null;
    }

    private static void createRetrofit(Context context) {

        OkHttpClient.Builder clientBuilder = new OkHttpClient().newBuilder();

        clientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder requestBuilder = request.newBuilder()
                        .header("authorization", "32DFCFD@#&DSFDSFSDF!L@?hh7@32DF");

                return chain.proceed(requestBuilder.build());
            }
        });

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);
        OkHttpClient client = clientBuilder.readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        mRetrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(HOST)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

}
