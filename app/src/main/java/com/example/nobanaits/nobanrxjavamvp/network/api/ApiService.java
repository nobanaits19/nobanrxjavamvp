package com.example.nobanaits.nobanrxjavamvp.network.api;

import com.example.nobanaits.nobanrxjavamvp.model.ClientList;

import io.reactivex.Observable;
import retrofit2.http.GET;
public interface ApiService {

    @GET("v2/client")
    Observable<ClientList> getClients();
}
