package com.example.nobanaits.nobanrxjavamvp.ui.Contract;

import com.example.nobanaits.nobanrxjavamvp.model.ClientList;

public interface MainActivityContract {
    interface  view{
        void showToast(String s);
        void displayClients(ClientList clientList);
        void displayError(String s);
    }
    interface presenter{
        void getClient();
    }
}
